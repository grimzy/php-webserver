# PHP built-in web server
Extremely simple usage of the PHP built-in web server.

## Usage
### Starting the web server
```bash
php -S localhost:8000
```

### Access from remote machines

```shell
php -S 0.0.0.0:8000
```

## Project specific PHP configuration
```shell
php -S localhost:8000 -c php.ini
```

## Usage with Composer

composer.json:

```json
"scripts": {
  "run": [
    "echo 'Started web server on http://localhost:8000'",
    "php -S localhost:8000"
  ]
}
```

And then run the web server with:
```shell
composer run-script run
```
